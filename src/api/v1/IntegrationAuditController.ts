import { Controller, Post, Body } from "@nestjs/common";
import { IntegrationAuditCore } from "../../core/IntegrationAuditCore";
import { IntegrationEmployeeInfo } from "../../domain/model/IntegrationEmployeeInfo";

@Controller('integration')
export class IntegrationAuditController {

    constructor(
        private readonly core: IntegrationAuditCore
    ) {}

    @Post('save-employee-info')
    async saveEmployeeInfo(
        @Body('message') message: string
    ) {
        return this.core.saveIntegrationEmployeeInfo(message)
    }
}