import { Injectable } from "@nestjs/common";
import { SaveIntegrationAuditService } from "../service/integration/SaveIntegrationAuditService";
import { IntegrationEmployeeInfo } from "../domain/model/IntegrationEmployeeInfo";

@Injectable()
export class IntegrationAuditCore {

    constructor(
        private readonly saveService: SaveIntegrationAuditService 
    ) {}

    async saveIntegrationEmployeeInfo(message: string) {
        const bufferedMessage = JSON.parse(Buffer.from(message, 'base64').toString())
        const info: IntegrationEmployeeInfo = { ...bufferedMessage }
        return this.saveService.save(info)
    }
}