import { Injectable } from "@nestjs/common";
import { IntegrationAuditRepository } from "../../repository/integration/IntegrationAuditRepository";
import { IntegrationEmployeeInfo } from "../../domain/model/IntegrationEmployeeInfo";

@Injectable()
export class SaveIntegrationAuditService {

    constructor(
        private readonly repository: IntegrationAuditRepository
    ) {}

    async save(info: IntegrationEmployeeInfo) {
        await this.repository.create(info)
        return info
    }
}