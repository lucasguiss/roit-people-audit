/**
 * @api {post} /api/v1/integration/save-employee-info Save Employee Info
 * @apiName Save Employee Info
 * @apiGroup Integration
 * @apiVersion 1.0.0
 * @apiParam {String} message Base 64 message
 * @apiDescription Creates a new Employee Info
 * @apiExample Example usage:
 *   <<base_url>>/api/v1/integration/save-employee-info 
 * @apiSuccessExample Success-Request:
 * HTTP/1.1 200 OK
 * {
 *  "message": "ewogICAgImN1c3RvbWVySWQiOiAic3RyaW5nIiwKICAgICJjb21wYW55Q29kZSI6IDEyMywKICAgICJjb21wYW55TmFtZSI6ICJzdHJpbmciLAogICAgImNucGoiOiAic3RyaW5nIiwKICAgICJjcmVhdGlvbkRhdGUiOiAic3RyaW5nIiwKICAgICJ1cGRhdGVEYXRlIjogInN0cmluZyIsCiAgICAiaW50ZWdyYXRpb25JZCI6ICJzdHJpbmciLAogICAgInNjYWxlQ29kZSI6ICJzdHJpbmciLAogICAgImNwZiI6ICJzdHJpbmciLAogICAgInVzZXJJZCI6ICJzdHJpbmciLAogICAgInVzZXJBZGRyZXNzSWQiOiAic3RyaW5nIiwKICAgICJwZXJtaXNzaW9ucyI6ICJzdHJpbmciLAogICAgImVtcGxveWVlSWQiOiAic3RyaW5nIiwKICAgICJqb3VybmV5SWQiOiAic3RyaW5nIiwKICAgICJjb25maWdJZCI6ICJzdHJpbmciLAogICAgInN0YXR1cyI6ICJISVJFRCIsCiAgICAiYXVkaXRlZCI6IHRydWUgCn0="
 * }
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *  "customerId": "string",
 *  "companyCode": 123,
 *  "companyName": "string",
 *  "cnpj": "string",
 *  "creationDate": "string",
 *  "updateDate": "string",
 *  "integrationId": "string",
 *  "scaleCode": "string",
 *  "cpf": "string",
 *  "userId": "string",
 *  "userAddressId": "string",
 *  "permissions": "string",
 *  "employeeId": "string",
 *  "journeyId": "string",
 *  "configId": "string",
 *  "status": "HIRED",
 *  "audited": true
 *}
 */