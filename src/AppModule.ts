import { Module } from '@nestjs/common';
import { IntegrationAuditModule } from './modules/IntegrationAuditModule';
import { LogsPontoModule } from './modules/LogsPontoModule';

@Module({
  imports: [
    IntegrationAuditModule,
    LogsPontoModule
  ]
})
export class AppModule {}
