export enum EmployeeStatusType {
    HIRED = 'HIRED',
    FIRED = 'FIRED'
}