import { EmployeeStatusType } from "../enum/EmployeeStatusType"

export class IntegrationEmployeeInfo {
    customerId: string
    companyCode: number
    companyName: string
    cnpj: string
    creationDate: string
    updateDate: string
    integrationId: string
    scaleCode: string
    cpf: string
    userId: string
    userAddressId: string
    permissions: string[]
    employeeId: string
    journeyId: string
    configId: string
    status: EmployeeStatusType
    audited: boolean 
}