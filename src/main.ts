import { NestFactory } from '@nestjs/core';
import { AppModule } from './AppModule';
import { Environment } from "roit-environment"
import { Logger } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix('api/v1')
  await app.listen(Environment.getProperty('port'));
  Logger.log(`App listening on port ${Environment.getProperty('port')}`)
  Logger.log(`Using credential ${Environment.getProperty('firestoreCredential{5}')}`)
}
bootstrap();
