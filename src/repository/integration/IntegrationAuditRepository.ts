import { Injectable } from "@nestjs/common";
import { BaseRepository } from "@roit/roit-firestore-connector";
import { IntegrationEmployeeInfo } from "../../domain/model/IntegrationEmployeeInfo";

@Injectable()
export class IntegrationAuditRepository extends BaseRepository<IntegrationEmployeeInfo>{

    constructor() {
        super('integrationEmployeeInfo')
    }
}