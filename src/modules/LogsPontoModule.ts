import { Module } from '@nestjs/common';
import { LogsPontoController } from '../api/v1/LogsPontoController';
import { LogsPontoCore } from '../core/LogsPontoCore';
import { SaveDailyRegisterLogs } from '../service/logs/SaveDailyRegisterLogs';
import { DailyRegisterLogsRepository } from '../repository/logs/DailyRegisterLogsRepository';

@Module({
    controllers: [LogsPontoController],
    providers: [
        LogsPontoCore,
        SaveDailyRegisterLogs,
        DailyRegisterLogsRepository
    ],
})
export class LogsPontoModule { }
