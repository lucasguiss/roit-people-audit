import { Module } from '@nestjs/common';
import { IntegrationAuditController } from '../api/v1/IntegrationAuditController';
import { IntegrationAuditCore } from '../core/IntegrationAuditCore';
import { SaveIntegrationAuditService } from '../service/integration/SaveIntegrationAuditService';
import { IntegrationAuditRepository } from '../repository/integration/IntegrationAuditRepository';

@Module({
  controllers: [IntegrationAuditController],
  providers: [
      IntegrationAuditCore,
      SaveIntegrationAuditService,
      IntegrationAuditRepository
  ],
})
export class IntegrationAuditModule {}
