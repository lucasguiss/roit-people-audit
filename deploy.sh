##HOM
gcloud builds submit --tag gcr.io/roit-people-hom/people-audit --project roit-people-hom
gcloud beta run deploy people-audit --image gcr.io/roit-people-hom/people-audit --platform managed --set-env-vars ENV=hom --memory=2G --timeout=540s --project roit-people-hom --allow-unauthenticated --region us-central1