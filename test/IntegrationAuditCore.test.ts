import { Test } from "@nestjs/testing"
import { AppModule } from "../src/AppModule"
import { IntegrationAuditCore } from "../src/core/IntegrationAuditCore"
import { SaveIntegrationAuditService } from "../src/service/integration/SaveIntegrationAuditService"
const mockedEmployeeInfo = async () => JSON.parse(JSON.stringify((await import('./mocks/MockedEmployeeInfo')).default))
const mockedEmployeeInfoMessage = async () => JSON.parse(JSON.stringify((await import('./mocks/MockedEmployeeInfoMessage')).default))

describe('IntegrationAuditCore tests', () => {

    let integrationAuditCore: IntegrationAuditCore
    let saveIntegrationAuditService: SaveIntegrationAuditService

    beforeEach(async () => {
        const testModule = await Test.createTestingModule({
            imports: [AppModule]
        }).compile()

        integrationAuditCore = testModule.get<IntegrationAuditCore>(IntegrationAuditCore)
        saveIntegrationAuditService = testModule.get<SaveIntegrationAuditService>(SaveIntegrationAuditService)
    })

    it('should create a employee info', async () => {
        const { message } = await mockedEmployeeInfoMessage()
        const info = await mockedEmployeeInfo()

        jest.spyOn(saveIntegrationAuditService, 'save').mockImplementation(() => Promise.resolve(info))

        const result = await integrationAuditCore.saveIntegrationEmployeeInfo(message)
        expect(result).toStrictEqual(info)
    })
})