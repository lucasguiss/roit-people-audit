export default {
    "customerId": "string",
    "companyCode": 123,
    "companyName": "string",
    "cnpj": "string",
    "creationDate": "string",
    "updateDate": "string",
    "integrationId": "string",
    "scaleCode": "string",
    "cpf": "string",
    "userId": "string",
    "userAddressId": "string",
    "permissions": "string",
    "employeeId": "string",
    "journeyId": "string",
    "configId": "string",
    "status": "HIRED",
    "audited": true
}