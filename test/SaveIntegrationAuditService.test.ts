import { Test } from "@nestjs/testing"
import { AppModule } from "../src/AppModule"
import { IntegrationAuditRepository } from "../src/repository/integration/IntegrationAuditRepository"
import { SaveIntegrationAuditService } from "../src/service/integration/SaveIntegrationAuditService"
const mockedEmployeeInfo = async () => JSON.parse(JSON.stringify((await import('./mocks/MockedEmployeeInfo')).default))

describe('SaveIntegrationAuditService tests', () => {

    let saveIntegrationAuditService: SaveIntegrationAuditService
    let repository: IntegrationAuditRepository

    beforeEach(async () => {
        const testModule = await Test.createTestingModule({
            imports: [AppModule]
        }).compile()

        repository = testModule.get<IntegrationAuditRepository>(IntegrationAuditRepository)
        saveIntegrationAuditService = testModule.get<SaveIntegrationAuditService>(SaveIntegrationAuditService)

    })
    
    it ('should insert in table', async () => {
        const mockedInfo = await mockedEmployeeInfo()
        jest.spyOn(repository, 'create').mockImplementation(() => Promise.resolve(mockedInfo))
        await saveIntegrationAuditService.save(mockedInfo)

        expect(repository.create).toBeCalledTimes(1)
    })
})